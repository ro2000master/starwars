import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {HttpClientModule} from '@angular/common/http';

import {PageNotFoundComponent} from './page-not-found/page-not-found.component';
import {StarshipsModule} from './starships/starships.module';
import {PeopleModule} from './people/people.module';
import {Router} from '@angular/router';

@NgModule({
    declarations: [
        AppComponent,
        PageNotFoundComponent,
    ],
    imports: [
        BrowserModule,
        HttpClientModule,

        StarshipsModule,
        PeopleModule,
        AppRoutingModule,
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {
    // constructor(router: Router) {
    //     const replacer = (key, value) => (typeof value === 'function') ? value.name : value;
    //     console.log('Routes: ', JSON.stringify(router.config, replacer, 2));
    // }
}
