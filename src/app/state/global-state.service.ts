import {Injectable} from '@angular/core';
import {ComponentStore} from '@ngrx/component-store';
import {Starship} from '../starships/starship';
import {switchMap, tap} from 'rxjs/operators';
import {StarshipService} from '../api/starship.service';
import {Person} from '../people/person';
import {PeopleService} from '../api/people.service';

interface IState {
    starships: Starship[];
    people: Person[];
}
const initialState: IState = {
    starships: [],
    people: [],
};

@Injectable({
    providedIn: 'root'
})
export class GlobalStateService extends ComponentStore<IState> {

    constructor(
        private starshipApi: StarshipService,
        private peopleApi: PeopleService
    ) {
        super(initialState);
    }

    public setStarships = this.updater<Starship[]>((state, value) => ({
       ...state,
       starships: value,
    }));

    public setPeople = this.updater<Person[]>((state, value) => ({
       ...state,
       people: value,
    }));

    public selectStarships$ = this.select<Starship[]>((state) => state.starships);
    public selectPeople$ = this.select<Person[]>((state) => state.people);

    /**********************/

    public loadStarships$ = this.effect<void>((trigger$) => {
       return trigger$
           .pipe(
               switchMap(() => this.starshipApi.getAllStarships().pipe(
                   tap((starships) => this.setStarships(starships))
               ))
           );
    });
    public loadPeople$ = this.effect<void>((trigger$) => {
       return trigger$
           .pipe(
               switchMap(() => this.peopleApi.getAllPeople().pipe(
                   tap((people) => this.setPeople(people))
               ))
           );
    });
}


