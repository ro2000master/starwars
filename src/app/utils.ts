import {Injectable} from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class Utils {
    public getIdFromUrl(url: string): string {
        return url.match(/\d+/)[0];
    }
}
