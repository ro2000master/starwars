import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {StarshipListComponent} from './starship-list/starship-list.component';
import {StarshipDetailComponent} from './starship-detail/starship-detail.component';
import {StarshipRoutingModule} from './starship-routing.module';

@NgModule({
    imports: [
        CommonModule,
        StarshipRoutingModule
    ],
    declarations: [
        StarshipListComponent,
        StarshipDetailComponent,
    ],
})
export class StarshipsModule {
}
