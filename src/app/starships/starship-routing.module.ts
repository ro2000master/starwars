import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {StarshipListComponent} from './starship-list/starship-list.component';
import {StarshipDetailComponent} from './starship-detail/starship-detail.component';

const starshipRoutes: Routes = [
    {path: 'starships', component: StarshipListComponent},
    {path: 'starship/:id', component: StarshipDetailComponent},
];

@NgModule({
    imports: [
        RouterModule.forChild(starshipRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class StarshipRoutingModule {
}
