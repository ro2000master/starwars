import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {Starship} from '../starship';
import {Observable} from 'rxjs';
import {GlobalStateService} from '../../state/global-state.service';

@Component({
    selector: 'app-starships',
    templateUrl: './starship-list.component.html',
    styleUrls: ['./starship-list.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class StarshipListComponent implements OnInit {

    public starships$: Observable<Starship[]>;

    constructor(
        private state: GlobalStateService
    ) {
    }

    public ngOnInit(): void {
        this.starships$ = this.state.selectStarships$;
    }
}
