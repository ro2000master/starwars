import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {combineLatest, Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {GlobalStateService} from '../../state/global-state.service';
import {Starship} from '../starship';
import {Utils} from '../../utils';

@Component({
    selector: 'app-starship-spec',
    templateUrl: './starship-detail.component.html',
    styleUrls: ['./starship-detail.component.scss']
})
export class StarshipDetailComponent implements OnInit {

    public starship$: Observable<Starship>;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private state: GlobalStateService,
        private utils: Utils,
    ) {
    }

    ngOnInit(): void {
        this.starship$ = combineLatest([
            this.route.paramMap.pipe(map(urlParam => urlParam.get('id'))),
            this.state.selectStarships$,
        ]).pipe(
            map(([id, starships]) => starships.find(item => item.id === id))
        );
    }

    public getPilotName(url: string): Observable<string> {
        const id = this.utils.getIdFromUrl(url);
        return this.state.selectPeople$.pipe(map((people) => people.find(item => item.id === id).name));
    }
}
