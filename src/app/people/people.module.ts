import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {PeopleDetailComponent} from './people-detail/people-detail.component';
import {PeopleListComponent} from './people-list/people-list.component';
import {PeopleRoutingModule} from './people-routing.module';

@NgModule({
    imports: [
        CommonModule,
        PeopleRoutingModule
    ],
    declarations: [
        PeopleDetailComponent,
        PeopleListComponent,
    ],
})
export class PeopleModule {
}
