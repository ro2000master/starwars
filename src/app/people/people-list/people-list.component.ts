import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {GlobalStateService} from '../../state/global-state.service';
import {Person} from '../person';

@Component({
    selector: 'app-starships',
    templateUrl: './people-list.component.html',
    styleUrls: ['./people-list.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PeopleListComponent implements OnInit {

    public people$: Observable<Person[]>;

    constructor(
        private state: GlobalStateService
    ) {
    }

    public ngOnInit(): void {
        this.people$ = this.state.selectPeople$;
    }
}
