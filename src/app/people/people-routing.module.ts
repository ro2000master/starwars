import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {PeopleListComponent} from './people-list/people-list.component';
import {PeopleDetailComponent} from './people-detail/people-detail.component';

const starshipRoutes: Routes = [
    {path: 'people', component: PeopleListComponent},
    {path: 'person/:id', component: PeopleDetailComponent},
];

@NgModule({
    imports: [
        RouterModule.forChild(starshipRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class PeopleRoutingModule {
}
