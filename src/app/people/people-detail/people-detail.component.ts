import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {combineLatest, Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {GlobalStateService} from '../../state/global-state.service';
import {Person} from '../person';
import {Utils} from '../../utils';

@Component({
    selector: 'app-starship-spec',
    templateUrl: './people-detail.component.html',
    styleUrls: ['./people-detail.component.scss']
})
export class PeopleDetailComponent implements OnInit {

    public person$: Observable<Person>;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private state: GlobalStateService,
        private utils: Utils,
    ) {
    }

    ngOnInit(): void {
        this.person$ = combineLatest([
            this.route.paramMap.pipe(map(urlParam => urlParam.get('id'))),
            this.state.selectPeople$,
        ]).pipe(
            map(([id, peaple]) => peaple.find(item => item.id === id))
        );
    }

    public getStarshipName(url: string): Observable<string> {
        const id = this.utils.getIdFromUrl(url);
        return this.state.selectStarships$.pipe(map((starships) => starships.find(item => item.id === id).name));
    }
}
