import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {GlobalStateService} from './state/global-state.service';
import {Observable} from 'rxjs';
import {Starship} from './starships/starship';
import {Person} from './people/person';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppComponent implements OnInit{

    public starships$: Observable<Starship[]>;
    public people$: Observable<Person[]>;

    constructor(private state: GlobalStateService) {
    }

    public ngOnInit(): void {
        this.state.loadStarships$();
        this.state.loadPeople$();
        this.starships$ = this.state.selectStarships$;
        this.people$ = this.state.selectPeople$;
    }
}
