import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BehaviorSubject, iif, Observable, of} from 'rxjs';
import {concatMap, map} from 'rxjs/operators';
import {Starship, StarshipsPage} from '../starships/starship';
import {Utils} from '../utils';


@Injectable({
    providedIn: 'root'
})
export class StarshipService {

    private starshipsUrl = 'https://swapi.dev/api/starships/';

    private httpOptions: {
        responseType: 'json',
        observe: 'body',
    };

    constructor(
        private http: HttpClient,
        private utils: Utils,
    ) {
    }

    public getAllStarships(): Observable<Starship[]> {
        return this.getStarshipsPage(this.starshipsUrl)
            .pipe(
                map(res => res.data),
                map(starships => {
                    return starships.map((starship) => {
                        starship.id = this.utils.getIdFromUrl(starship.url);
                        return starship;
                    });
                })
            );
    }

    private getStarshipsPage(url: string, acum: Starship[] = []): Observable<{ data: Starship[], next: string }> {
        return this.http.get<StarshipsPage>(url, this.httpOptions)
            .pipe(
                map(data => ({data: [...acum, ...data.results], next: data.next})),
                concatMap(res => iif(() => !!res.next, this.getStarshipsPage(res.next, res.data), of(res)))
            );
    }


}
