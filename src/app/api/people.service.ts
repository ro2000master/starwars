import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BehaviorSubject, iif, Observable, of} from 'rxjs';
import {concatMap, map} from 'rxjs/operators';
import {Starship, StarshipsPage} from '../starships/starship';
import {Person, PeoplePage} from '../people/person';
import {Utils} from '../utils';


@Injectable({
    providedIn: 'root'
})
export class PeopleService {

    private peopleUrl = 'https://swapi.dev/api/people/';

    private httpOptions: {
        responseType: 'json',
        observe: 'body',
    };

    constructor(
        private http: HttpClient,
        private utils: Utils,
    ) {
    }

    public getAllPeople(): Observable<Person[]> {
        return this.getPeoplePage(this.peopleUrl)
            .pipe(
                map(res => res.data),
                map(peaple => {
                    return peaple.map((person, index) => {
                        person.id = this.utils.getIdFromUrl(person.url);
                        return person;
                    });
                })
            );
    }

    private getPeoplePage(url: string, acum: Person[] = []): Observable<{ data: Person[], next: string }> {
        return this.http.get<PeoplePage>(url, this.httpOptions)
            .pipe(
                map(data => ({data: [...acum, ...data.results], next: data.next})),
                concatMap(res => iif(() => !!res.next, this.getPeoplePage(res.next, res.data), of(res)))
            );
    }
}
